<?php
require(dirname(__FILE__) . '/../models/qApiKey.php');

class m000000_100002_qApi_data extends CDbMigration
{
	public function safeUp()
	{
		$this->insert('qApi_key', array(
			'user_id' => '1',
			'key' => qApiKey::model()->generateKey(),
			'secret' => qApiKey::model()->generateSecret(),
			'description' => 'Test API Key',
			'create_time' => time(),
			'update_time' => time(),
			'state' => 'active',
		));
		$this->insert('qApi_settings', array(
			'type' => 'security',
			'attribute' => 'key',
			'label' => 'API Key',
			'value' => 'false',
			'state' => 'active',
		));
		$this->insert('qApi_settings', array(
			'type' => 'security',
			'attribute' => 'token',
			'label' => 'API Token',
			'value' => 'false',
			'state' => 'active',
		));
	}

	public function safeDown()
	{
		$this->delete('qApi_settings', 'attribute="token"');
		$this->delete('qApi_settings', 'attribute="key"');
		$this->delete('qApi_key', 'description="Test API Key"');
	}
}