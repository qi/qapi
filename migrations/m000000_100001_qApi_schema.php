<?php

class m000000_100001_qApi_schema extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('qApi_key', array(
			'id' => 'pk',
			'user_id' => 'integer NOT NULL',
			'key' => 'string NOT NULL',
			'secret' => 'string NOT NULL',
			'description' => 'text',
			'create_time' => 'integer UNSIGNED NOT NULL',
			'update_time' => 'integer UNSIGNED NOT NULL',
			'state' => 'string NOT NULL',
			'key (`key`)'
			//'foreign key (`user_id`) references `tbl_user` (`id`) on delete cascade on update cascade'
		));
		$this->createTable('qApi_log', array(
			'id' => 'pk',
			'client_addr' => 'string',
			'client_key' => 'string',
			'client_token' => 'string',
			'request_type' => 'string',
			'request_uri' => 'text',
			'request_headers' => 'text',
			'request_body' => 'text',
			'request_time' => 'double',
			'response_code' => 'integer',
			'response_status' => 'string',
			'response_headers' => 'text',
			'response_body' => 'text',
			'profiling_time' => 'float',
			'profiling_memory' => 'integer',
			'foreign key (`client_key`) references `qApi_key` (`key`)'
		));
		$this->createTable('qApi_settings', array(
			'id' => 'pk',
			'type' => 'string NOT NULL',
			'label' => 'string NOT NULL',
			'attribute' => 'string NOT NULL',
			'value' => 'text NOT NULL',
			'state' => 'string NOT NULL',
		));
	}

	public function safeDown()
	{
		$this->dropTable('qApi_settings');
		$this->dropTable('qApi_log');
		$this->dropTable('qApi_key');
	}
}