<?php

/**
 * Class qApiToken
 * @todo session description
 * @author andcherv <andcherv@gmail.com>
 * @property string $token
 * @property integer $expire
 */
class qApiToken extends CApplicationComponent
{
	/**
	 * Allocation strategy based on token's {@see $lifetime}.
	 * Token regenerates on authorization and is valid during its lifetime.
	 * When token expires re-authorization is required.
	 * This strategy is suitable if your API processes multiple simultaneous requests.
	 */
	const ALLOCATION_PERIOD = 'period';
	/**
	 * One-time token allocation strategy. API provides per-request unique token.
	 * This strategy is more secure, but requires sequential execution of requests,
	 * which can cause performance issues.
	 */
	const ALLOCATION_REQUEST = 'request';
	/**
	 * Current token allocation strategy. Default to {@see ALLOCATION_PERIOD}.
	 * @var string
	 */
	public $allocationStrategy = self::ALLOCATION_PERIOD;
	/**
	 * Token lifetime used for {@see ALLOCATION_PERIOD} strategy.
	 * @var int
	 */
	public $lifetime = 3600;

	/**
	 * Verificates a token according to the current allocation strategy.
	 * @return bool
	 */
	public function verify()
	{
		if (isset(Yii::app()->request->cookies[qApiModule::COOKIE_TOKEN_NAME])) {
			$requestToken = Yii::app()->request->cookies[qApiModule::COOKIE_TOKEN_NAME];
			if ($requestToken == Yii::app()->user->getState('__token')) {
				switch ($this->allocationStrategy) {
					case self::ALLOCATION_PERIOD:
						$expire = Yii::app()->user->getState('__tokenExpire');
						if (isset($expire) && time() <= $expire)
							return true;
						break;
					case self::ALLOCATION_REQUEST:
						Yii::app()->user->setState('__token', $this->regenerate());
						return true;
						break;
				}
			}
		}
		return false;
	}

	/**
	 * Returns a token.
	 * @return mixed
	 */
	public function getToken()
	{
		if (Yii::app()->user->getState('__token') === null)
			Yii::app()->user->setState('__token', $this->regenerate());
		return Yii::app()->user->getState('__token');
	}

	/**
	 * Returns token expire time.
	 * @return mixed
	 */
	public function getExpire()
	{
		if (Yii::app()->user->getState('__tokenExpire') === null)
			Yii::app()->user->setState('__tokenExpire', time() + $this->lifetime);
		return Yii::app()->user->getState('__tokenExpire');
	}

	/**
	 * Refenerates a token and sets cookies.
	 * @return null|string
	 */
	protected function regenerate()
	{
		$token = sha1(time() . rand());
		$cookie = new CHttpCookie(qApiModule::COOKIE_TOKEN_NAME, $token, array(
			'path' => qApiModule::COOKIE_PATH,
			'secure' => false,
			'httpOnly' => true,
			'expire' => time() + qApiModule::SESSION_LIFETIME,
		));
		if ($this->allocationStrategy == self::ALLOCATION_PERIOD)
			Yii::app()->user->setState('__tokenExpire', time() + $this->lifetime);
		if (Yii::app()->request->cookies[qApiModule::COOKIE_TOKEN_NAME] = $cookie)
			return $token;
		return null;
	}
}