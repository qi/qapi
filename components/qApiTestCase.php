<?php

/**
 * Class qApiTestCase
 * @todo request class??
 * @fixme refactor and optimize
 * @author andcherv
 */
abstract class qApiTestCase extends CTestCase
{
	protected $_host = 's-kid.dev/api';
//	protected $_host = 'v2.s-kid.net-pupil.com/api';
	protected $_resource = '';
	protected $_apiKey = '3ccb7144e02e902a7c19022fb5836900643a44e0';
	protected $_apiSecret = 'decBAyi2ll0g4is8';
	protected $_session;
	protected $_token;
	protected $_user;
	protected $_responseCode;
	protected $_responseHeaders;
	protected $_responseBody;

	protected function curl($params, $sendAuth = true, $sendCookie = true)
	{
		// setting the method
		if (isset($params['method']))
			$method = $params['method'];
		else $method = 'GET';

		// setting the url
		$url = 'http://' . $this->_host;
		if (isset($params['resource']))
			$url .= $params['resource'];
		else
			$url .= $this->_resource;
		if (isset($params['query']))
			$url .= $params['query'];

		// setting the body
		if (isset($params['data']) && !empty($params['data']))
			$body = CJSON::encode($params['data']);
		else $body = null;

		// setting headers array
		$headers = array(
			"Accept: application/json",
			"Content-Type: application/json",
		);
		if ($sendAuth === true) {
			$headers = CMap::mergeArray($headers, array(
				"Auth-Key: {$this->_apiKey}",
				"Auth-Hash: {$this->genApiHash($body, $url)}",
			));
		}
		if ($sendCookie === true && (isset($this->_session) && isset($this->_token))) {
			$headers = CMap::mergeArray($headers, array(
				"Cookie: QAPISESSID={$this->_session};QAPITOKEN={$this->_token};",
			));
		}
		if (isset($params['headers']) && !empty($params['headers'])) {
			$headers = CMap::mergeArray($headers, $params['headers']);
		}

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		if ($method == 'HEAD')
			curl_setopt($curl, CURLOPT_NOBODY, true);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HEADER, true);
		$response = curl_exec($curl);
		list($this->_responseHeaders, $this->_responseBody) = explode("\r\n\r\n", $response, 2);
		$this->_responseCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		curl_close($curl);

		preg_match_all('/Set-Cookie: QAPITOKEN=(\w{1,});/', $response, $tokens);
		foreach ($tokens[1] as $token) {
			if (isset($token) && !empty($token))
				$this->_token = $token;
			elseif ($token == 'deleted') {
				$this->_token = null;
			}
		}

		return $this->_responseBody;
	}

	private function genApiHash($body, $url)
	{
		return hash_hmac('sha1',
			$this->_apiKey .
			((int)(time() / 1000) * 1000) .
			$body . $this->_token . $url,
			$this->_apiSecret);
	}

	protected function login($user = 'student10')
	{
		$users = array(
			'student10' => array(
				'login' => 'student10@i-pupils.com',
				'password' => 'student10',
			),
			'student11' => array(
				'login' => 'student11@i-pupils.com',
				'password' => 'student11',
			),
			'student20' => array(
				'login' => 'student20@i-pupils.com',
				'password' => 'student20',
			),
		);
		$auth = CJSON::decode($this->curl(array(
			'method' => 'POST',
			'resource' => '/auth/',
			'data' => $users[$user],
		)));
		$this->assertNotNull($auth);
		$this->assertArrayHasKey('status', $auth);
		$this->assertEquals('success', $auth['status']);
		$this->assertArrayHasKey('session', $auth['data']);
		$this->assertNotEmpty('session', $auth['data']);
		$this->assertArrayHasKey('token', $auth['data']);
		$this->assertNotEmpty('token', $auth['data']);
		$this->assertArrayHasKey('user', $auth['data']);
		$this->assertArrayHasKey('public_id', $auth['data']['user']);
		$this->assertNotEmpty('public_id', $auth['data']['user']);
		$this->_session = $auth['data']['session'];
		$this->_token = $auth['data']['token'];
		$this->_user = $auth['data']['user']['public_id'];
	}

	protected function logout()
	{
		$response = CJSON::decode($this->curl(array(
			'method' => 'DELETE',
			'resource' => '/auth/' . $this->_user,
		)));
		$this->assertArrayHasKey('status', $response);
		$this->assertEquals('success', $response['status']);
		unset($this->_session);
		unset($this->_token);
		unset($this->_user);
	}

}