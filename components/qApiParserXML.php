<?php

/**
 * qApi XML Parser.
 * @todo handle fail responses
 * @author andcherv
 */
class qApiParserXML extends qApiParser
{
	public function getTypes()
	{
		return array(
			'xml',
			'application/xml'
		);
	}

	public function serialize($body)
	{
		return self::encode($body);
	}

	// TODO: handle xml structure warnings
	public function parse($data)
	{
		return $parse = json_decode(json_encode((array)simplexml_load_string($data)), 1);
	}

	/**
	 * Encodes PHP array to XML.
	 * @param $data
	 * @return mixed
	 */
	public static function encode($data)
	{
		$sxe = new SimpleXMLElement("<root/>");
		self::reEncode($sxe, $data);
		return $sxe->asXML();
	}

	/**
	 * Encodes arrays recursively.
	 * @param $sxe
	 * @param $data
	 * @return mixed
	 */
	private static function reEncode($sxe, $data)
	{
		foreach ($data as $key => $value) {
			if (is_array($value)) {
				if (is_numeric($key)) {
					$subsxe = $sxe->addChild("item-$key");
					self::reEncode($subsxe, $value);
				} else {
					$subsxe = $sxe->addChild("$key");
					self::reEncode($subsxe, $value);
				}
			} else {
				$sxe->addChild($key, $value);
			}
		}
		return $sxe->asXML();
	}
}