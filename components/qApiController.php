<?php

/**
 * Class qApiController
 * TODO Define filter in config?
 * TODO: override POST etc responses
 * @author chervand <chervand@gmail.com>
 * @link http://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html
 * @property null|string $modelName
 * @property qApiModule $module
 * @property qApiRequest $request
 * @property qApiResponse $response
 * @since v1.0.0
 */
abstract class qApiController extends Controller
{
	/**
	 * Default controller action.
	 * TODO: something throwing 405 or 501 instead (verbs not covered my urlManager rules)
	 * @var string
	 */
	public $defaultAction = 'OPTIONS';

	/**
	 * Allowed actions for the controller. All disallowed by default.
	 * TODO: throw 501 instead of 404 if no action is defined
	 * TODO: throw 405 if action is defined but don't allowed
	 * @var array
	 */
	public $allow = [];

	/**
	 * Returns actions mapping for a controller.
	 * Actions should be defined via actionMap module's attribute.
	 * Actions should be allowed via allow attribute for each controller.
	 * @return array
	 * @see qApiModule::actionMap
	 * @see allow
	 * @since 1.0.0
	 */
	public function actions()
	{
		return $this->module->actionMap ? $this->module->actionMap : parent::actions();
	}

	/**
	 * Returns a basic model used within a resource. Should be overriden
	 * in a corresponding controller. Is used in common actions.
	 * @author andcherv
	 * @return null|CActiveRecord Instance of a model class.
	 * @see $this->actions()
	 */
	public function model()
	{
		return null;
	}

	/**
	 * Returns criteria used in API methods for models search. Should be overriden
	 * in a corresponding controller.
	 * @author andcherv
	 * @return CDbCriteria
	 */
	public function criteria()
	{
		return new CDbCriteria();
	}

	/**
	 * Returns a data array for a GET method callback of the resource. Should be overriden
	 * in a corresponding controller.
	 * @author andcherv
	 * @param $model
	 * @return array
	 */
	public function data(CActiveRecord $model)
	{
		return $model->attributes;
	}

	/**
	 * Renders a response.
	 * @author andcherv
	 * @param string $response
	 * @param bool $return
	 * @return string|void
	 */
	public function render($response, $return = false)
	{
		return $this->module->response->render($response, $return);
	}

	public function renderSaveErrors($model, $exception = null)
	{
		if ($model->hasErrors()) {
			$this->render(array(
				'code' => 422,
				'body' => array(
					'status' => qApiResponse::STATUS_FAIL,
					'data' => $model->errors,
				),
			));
		} elseif (isset($exception)) {
			$this->render(array(
				'code' => 500,
				'body' => array(
					'status' => qApiResponse::STATUS_ERROR,
					'message' => $exception->getMessage(),
				),
			));
		} else {
			$this->render(array(
				'code' => 500,
				'body' => array(
					'status' => qApiResponse::STATUS_ERROR,
				),
			));
		}
	}

	public function renderInsertSuccess($model)
	{
		$this->render(array(
			'code' => 201,
			//@todo locaion
			'body' => array(
				'status' => qApiResponse::STATUS_SUCCESS,
				'data' => array(
					'id' => isset($model->id) ? $model->id : '',
				),
			),
		));
	}

	public function renderUpdateSuccess($model)
	{
		$this->render(array(
			'code' => 200,
			'body' => array(
				'status' => qApiResponse::STATUS_SUCCESS,
				'data' => array(
					'public_id' => isset($model->public_id) ? $model->public_id : '',
				),
			),
		));
	}

	public function renderDeleteSuccess()
	{
		$this->render(array(
			'code' => 200,
			'body' => array(
				'status' => qApiResponse::STATUS_SUCCESS,
			),
		));
	}
}