<?php

/**
 * Class qApiParser
 * @author andcherv
 * @since v1.1
 */
abstract class qApiParser extends CApplicationComponent
{

	/**
	 * Returns supported MIME types.
	 * @author andcherv
	 * @return array
	 * @since v1.1
	 */
	abstract public function getTypes();

	/**
	 * Serializes response body.
	 * @author andcherv
	 * @param array $body PHP array representing response body
	 * @return mixed Response bodyin a specified format
	 * @since v1.1
	 */
	abstract public function serialize($body);

	/**
	 * Parses request body.
	 * @author andcherv
	 * @param string $body Request body in a specified format
	 * @return array Request body, parsed to PHP array
	 * @since v1.1
	 */
	abstract public function parse($body);
}