<?php

/**
 * REST Response component.
 * @todo move response statuses to this component
 * @author andcherv
 * @property qApiModule $module
 * @property $code
 * @property $status
 * @property $headers
 * @property $contentType
 * @property $body
 */
class qApiResponse extends CApplicationComponent
{
	const HTTP_SUCCESS = 200;
	const HTTP_CREATED = 201;

	const STATUS_UNKNOWN = 'unknown';
	const STATUS_ERROR = 'error';
	const STATUS_FAIL = 'fail';
	const STATUS_SUCCESS = 'success';

	/**
	 * Select attributes set in config.
	 * @author andcherv
	 * @var
	 */
	public $select;
	/**
	 * Default select conditions set in config.
	 * @author andcherv
	 * @var
	 */
	public $condition;
	/**
	 * Response body.
	 * @author andcherv
	 * @var
	 */
	private $_body;
	private $_headers;
	private $_status;
	/**
	 * @var
	 */
	private $_contentType;

	/**
	 * Returns qApi module instance.
	 * @author andcherv
	 * @return bool
	 */
	public function getModule()
	{
		return Yii::app()->controller->module;
	}

	/**
	 * Returns the response status code.
	 * @author andcherv
	 * @return mixed
	 */
	public function getCode()
	{
		return function_exists('http_response_code') ? http_response_code() : null;
	}

	/**
	 * Returns the response status.
	 * @author andcherv
	 * @return string
	 */
	public function getStatus()
	{
		return isset($this->_status) ? $this->_status : self::STATUS_UNKNOWN;
	}

	public function getHeaders()
	{
		return isset($this->_headers) ? $this->_headers : null;
	}

	/**
	 * Returns response content type. If not set, returns on of the module's available formats.
	 * @author andcherv
	 * @return mixed
	 */
	public function getContentType()
	{
		if (!isset($this->_contentType))
			$this->_contentType = $this->module->defaultFormat;
		return $this->_contentType;
	}

	/**
	 * Sets response content type.
	 * @author andcherv
	 * @param $contentType
	 */
	public function setContentType($contentType)
	{
		$this->_contentType = $contentType;
	}

	/**
	 * Returns response body.
	 * @author andcherv
	 * @return null
	 */
	public function getBody()
	{
		if (isset($this->_body))
			return $this->_body;
		return null;
	}

	/**
	 * Return correct message for each known http error code
	 * @param integer $httpCode error code to map
	 * @param string $replacement replacement error string that is returned if code is unknown
	 * @return string the textual representation of the given error code or the replacement string if the error code is unknown
	 */
	public function getHttpHeader($httpCode, $replacement = '')
	{
		$httpCodes = array(
			100 => 'Continue',
			101 => 'Switching Protocols',
			102 => 'Processing',
			118 => 'Connection timed out',
			200 => 'OK',
			201 => 'Created',
			202 => 'Accepted',
			203 => 'Non-Authoritative',
			204 => 'No Content',
			205 => 'Reset Content',
			206 => 'Partial Content',
			207 => 'Multi-Status',
			210 => 'Content Different',
			300 => 'Multiple Choices',
			301 => 'Moved Permanently',
			302 => 'Found',
			303 => 'See Other',
			304 => 'Not Modified',
			305 => 'Use Proxy',
			307 => 'Temporary Redirect',
			310 => 'Too many Redirect',
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			405 => 'Method Not Allowed',
			406 => 'Not Acceptable',
			407 => 'Proxy Authentication Required',
			408 => 'Request Time-out',
			409 => 'Conflict',
			410 => 'Gone',
			411 => 'Length Required',
			412 => 'Precondition Failed',
			413 => 'Request Entity Too Large',
			414 => 'Request-URI Too Long',
			415 => 'Unsupported Media Type',
			416 => 'Requested range unsatisfiable',
			417 => 'Expectation failed',
			418 => 'I’m a teapot',
			422 => 'Unprocessable entity',
			423 => 'Locked',
			424 => 'Method failure',
			425 => 'Unordered Collection',
			426 => 'Upgrade Required',
			449 => 'Retry With',
			450 => 'Blocked by Windows Parental Controls',
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
			502 => 'Bad Gateway ou Proxy Error',
			503 => 'Service Unavailable',
			504 => 'Gateway Time-out',
			505 => 'HTTP Version not supported',
			507 => 'Insufficient storage',
			509 => 'Bandwidth Limit Exceeded',
		);
		if (isset($httpCodes[$httpCode]))
			return $httpCodes[$httpCode];
		else
			return $replacement;
	}

	/**
	 * Renders a response according to the current API format.
	 * It's recommended to follow {@link http://labs.omniti.com/labs/jsend jSend}
	 * specification for responses.
	 * @author andcherv
	 * @param array $response
	 * @param bool $return
	 * @return string|void
	 * @since v1.1
	 */
	public function render($response, $return = false)
	{
		if (isset($response['code']))
			$this->_setCode($response['code']);
		else $this->_setCode(self::HTTP_SUCCESS);

		if (isset($response['headers']))
			$this->_setHeaders($response['headers']);
		else $this->_setHeaders();

		if (isset($response['body']) && is_array($response['body'])) {
			if (isset($response['body']['status']))
				$this->_status = $response['body']['status'];
			$parser = $this->module->formats[$this->contentType];
			$this->_body = $this->module->{$parser}->serialize($response['body']);
		}

		if (isset($this->_body) && is_string($this->_body))
			echo $this->_body;
	}

	/**
	 * Returns default CDbCriteria according to config.
	 * @author andcherv
	 * @param $object
	 * @return CDbCriteria
	 */
	public function getConfigCriteria($object)
	{
		$criteria = new CDbCriteria();
		$criteria->select = $this->getSelect($object);
		$criteria->condition = $this->getCondition($object);
		$criteria->order = 't.id DESC';
		return $criteria;
	}

	/**
	 * Returns default select criteria according to config.
	 * @author andcherv
	 * @param null $object
	 * @return string
	 */
	public function getSelect($object = null)
	{
		$select = '*';
		if (isset($this->select) && !empty($this->select)) {
			if (isset(Yii::app()->controller) && !empty($this->select[Yii::app()->controller->id])) {
				if ($object !== null && isset($this->select[Yii::app()->controller->id][$object])) {
					$select = $this->select[Yii::app()->controller->id][$object];
				}
			}
		}
		return $select;
	}

	/**
	 * Returns default select condition according to config.
	 * @author andcherv
	 * @param null $object
	 * @return string
	 */
	public function getCondition($object = null)
	{
		$condition = '';
		if (isset($this->condition) && !empty($this->condition)) {
			if (isset(Yii::app()->controller) && !empty($this->condition[Yii::app()->controller->id])) {
				if ($object !== null && isset($this->condition[Yii::app()->controller->id][$object])) {
					$condition = $this->condition[Yii::app()->controller->id][$object];
				}
			}
		}
		return $condition;
	}

	private function _setCode($code)
	{
		if (!headers_sent()) {
			if (isset($code) && is_numeric($code)) {
				if (function_exists('http_response_code'))
					http_response_code($code);
				header("HTTP/1.1 {$code}");
			}
		}
	}

	private function _setHeaders(array $headers = array())
	{
		if (!headers_sent()) {

			if (isset($this->contentType))
				header("Content-type: {$this->contentType}; charset=UTF-8");
			else header('Content-type: text/plain; charset=UTF-8');

			if (isset($headers) && is_array($headers)) {
				foreach ($headers as $header => $value) {
					is_callable($value) ? $value = call_user_func($value) : false;
					is_string($header) && is_string($value) ? header("{$header}: {$value}") : false;
				}
			}

			foreach (headers_list() as $header) {
				$header = explode(': ', $header);
				if (count($header) == 2)
					$this->_headers[trim(reset($header))] = trim(end($header));
			}

		}
	}

}