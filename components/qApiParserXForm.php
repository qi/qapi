<?php

/**
 * qApi x-www-form-urlencoded format rarser.
 * @author andcherv
 */
class qApiParserXForm extends qApiParser
{
	public function getTypes()
	{
		return array(
			'application/x-www-form-urlencoded',
			'multipart/form-data'
		);
	}

	public function serialize($body)
	{
		return http_build_query($body);
	}

	public function parse($body)
	{
		parse_str($body, $body);
		return $body;
	}
}