<?php

/**
 * qApi JSON Parser.
 * @author andcherv
 */
class qApiParserJSON extends qApiParser
{
	public function getTypes()
	{
		return array(
			'json',
			'application/json'
		);
	}

	public function serialize($body)
	{
		return CJSON::encode($body);
	}

	public function parse($body)
	{
		return CJSON::decode($body);
	}
}