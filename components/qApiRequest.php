<?php

/**
 * REST request validation methods.
 * @author andcherv
 * @property qApiModule $module
 * @property $addr
 * @property $type
 * @property $uri
 * @property $headers
 * @property $authKey
 * @property $authHash
 * @property $accept
 * @property $contentType
 * @property $body
 * @property $time
 */
class qApiRequest extends CApplicationComponent
{
	/**
	 * Attributes vvalidation rules set in config.
	 * @author andcherv
	 * @var $validate
	 */
	public $validate;
	private $_headers;
	private $_accept;
	private $_contentType;
	private $_rawBody;
	private $_body;


	/*----------------------------------*/

	/**
	 * Returns qApi module instance.
	 * @author andcherv
	 * @return bool
	 */
	public function getModule()
	{
		return Yii::app()->controller->module;
	}

	public function getAddr()
	{
		if (isset($_SERVER['HTTP_X_REAL_IP']) && !empty($_SERVER['HTTP_X_REAL_IP']))
			$ip = $_SERVER['HTTP_X_REAL_IP'];
		elseif (isset($_SERVER['HTTP_CLIENT_IP']) && !empty($_SERVER['HTTP_CLIENT_IP']))
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && !empty($_SERVER['HTTP_X_FORWARDED_FOR']))
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		elseif (isset($_SERVER['REMOTE_ADDR']))
			$ip = $_SERVER['REMOTE_ADDR'];
		else $ip = '0.0.0.0';

		if (isset($_SERVER['REMOTE_PORT']) && !empty($_SERVER['REMOTE_PORT']))
			$port = $_SERVER['REMOTE_PORT'];
		else $port = '0';

		return "{$ip}:{$port}";
	}

	/**
	 * Returns the request type.
	 * @author andcherv
	 * @return string
	 */
	public function getType()
	{
		return Yii::app()->request->requestType;
	}

	/**
	 * Returns the request URI.
	 * @author andcherv
	 * @return mixed
	 */
	public function getUri()
	{
		$scheme = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
		$host = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : '';
		$uri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
		return "{$scheme}{$host}{$uri}";
	}

	/**
	 * Returns the request body.
	 * @author andcherv
	 * @return array|string
	 * @throws CHttpException
	 */
	public function getBody()
	{
		if (!isset($this->_body)) {
			if (isset($this->module->formats[$this->contentType])) {
				$parser = $this->module->formats[$this->contentType];
				if (isset($this->module->{$parser}))
					$this->_body = $this->module->{$parser}->parse($this->getRawBody());
				else throw new CHttpException(415);
			} // else throw new CHttpException(415);
		}
		return !empty($this->_body) ? $this->_body : $_POST;
	}

	public function setBody($attributes)
	{
		$this->_body = $attributes;
	}

	public function getRawBody()
	{
		if (!isset($this->_rawBody))
			$this->_rawBody = @file_get_contents('php://input');
		return !empty($this->_rawBody) ? $this->_rawBody : null;
	}

	public function getHeaders($serialized = false)
	{
		if (!isset($this->_headers)) {
			if (function_exists('getallheaders')) {
				$this->_headers = getallheaders() ? getallheaders() : [];
			} else {
				foreach ($_SERVER as $name => $value) {
					if (substr($name, 0, 5) == 'HTTP_') {
						$this->_headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
					} else if ($name == "CONTENT_TYPE") {
						$this->_headers["Content-Type"] = $value;
					} else if ($name == "CONTENT_LENGTH") {
						$this->_headers["Content-Length"] = $value;
					}
				}
			}
		}
		return $serialized === true ? serialize($this->_headers) : $this->_headers;
	}

	/**
	 * @todo optimize method returns
	 * @todo * defaults to json
	 * @return array
	 * @throws CHttpException
	 */
	public function getAccept()
	{
		if (!isset($this->_accept)) {
			if (isset($this->headers['Accept'])) {
				$accept = array_map('trim', explode(',', strtok($this->headers['Accept'], ';')));
				foreach ($accept as $format) {
					if (array_key_exists($format, $this->module->formats)) {
						return $this->_accept = $format;
					}
				}
			} else throw new CHttpException(400, 'Required HTTP header "Accept" is missing.');
		}
		if (!isset($this->_accept) && !isset($this->module->defaultFormat))
			throw new CHttpException(406);
		return $this->_accept;
	}

	public function getContentType($requestTypes = array('POST', 'PUT'))
	{
		if (!isset($this->_contentType)) {
			if (isset($this->headers['Content-Type']))
				$this->_contentType = strtok(strtok($this->headers['Content-Type'], ';'), ',');
			elseif (in_array($this->type, $requestTypes))
				throw new CHttpException(400, 'Required HTTP header "Content-Type" is missing.');
		}
		return $this->_contentType;
	}

	/**
	 * Returns request time.
	 * @author andcherv
	 * @return int
	 */
	public function getTime()
	{
		if (isset($_SERVER['REQUEST_TIME_FLOAT']))
			return (float)$_SERVER['REQUEST_TIME_FLOAT'];
		elseif (isset($_SERVER['REQUEST_TIME']))
			return (int)$_SERVER['REQUEST_TIME'];
		return (float)microtime(true);
	}

	public function getAuthKey()
	{
		if (isset($this->headers['Auth-Key']))
			return $this->headers['Auth-Key'];
		return null;
	}

	public function getAuthHash()
	{
		if (isset($this->headers['Auth-Hash']))
			return $this->headers['Auth-Hash'];
		return null;
	}

	public function getAuthToken()
	{
		return isset(Yii::app()->user->token) ? Yii::app()->user->token : null;
	}

	/**
	 * Verificates HTTP request authentication.
	 * Returns true in case no exceptions were thrown.
	 * @author andcherv
	 * @return bool|void
	 * @throws CHttpException
	 */
	public function verify()
	{
		foreach ($this->module->security as $security => $state) {
			if ($state == 'true') {
				switch ($security) {
					case 'ssl':
						if (!isset($_SERVER['HTTP_SSL_VERIFIED']) || $_SERVER['HTTP_SSL_VERIFIED'] != 'SUCCESS')
							throw new CHttpException(401, 'SSL verification failed.');
						break;
					case 'key':
						$key = qApiKey::model()->find(array(
							'condition' => '`t`.`key`=:key',
							'params' => array(':key' => $this->authKey),
						));
						$data = array(
							'body' => $this->getRawBody(),
							'token' => Yii::app()->controller->id == 'auth' && ($this->type == 'POST') ? '' : Yii::app()->user->token,
							'uri' => $this->uri,
						);
						if (!isset($key) || !$key->verifyHash($this->authHash, $data))
							throw new CHttpException(401, 'Authentication credentials were missing or incorrect.');
						break;
					case 'token':
						if (Yii::app()->controller->id != 'auth' && $this->module->token->verify() !== true)
							throw new CHttpException(401, 'Token mismatch. Reauthorization required.');
						break;
				}
			}
		}
		return true;
	}

	/*----------------------------------*/

	/**
	 * Validates attributes.
	 * @author andcherv
	 * @param array $attributes
	 * @return bool
	 */
	public function validateAttributes(array $attributes)
	{
		$this->validateRequiredAttributes($attributes);
		$this->validateAttributesFormat($attributes);
		return true;
	}

	/**
	 * Validates required attributes. Required attributes should be set to null, optional to false.
	 * @author andcherv
	 * @param $attributes
	 * @return bool
	 * @throws CHttpException
	 */
	public function validateRequiredAttributes(array $attributes)
	{
		$required = array();
		foreach ($attributes as $key => $value) {
			if ($value === null) {
				array_push($required, $key);
			}
		}
		if (!empty($required)) {
			$attributesString = implode(', ', $required);
			throw new CHttpException(400, "Required attributes: $attributesString.");
		}
		return true;
	}

	/**
	 * Validates attributes values formats, defined in config.
	 * @author andcherv
	 * @param array $attributes
	 * @return bool
	 * @throws CHttpException
	 */
	public function validateAttributesFormat(array $attributes)
	{
		if (isset($this->validate['numeric']) && !empty($this->validate['numeric'])) {
			$numeric = $this->validate['numeric'];
			$errors = array();
			foreach ($attributes as $key => $value) {
				if (in_array($key, $numeric) && $value !== null && $value !== false && !is_numeric($value)) {
					array_push($errors, $key);
				}
			}
			if (!empty($errors)) {
				$attributesString = implode(', ', $errors);
				throw new CHttpException(400, "Attributes must be numeric: $attributesString");
			}
		}
		if (isset($this->validate['values']) && !empty($this->validate['values'])) {
			foreach ($this->validate['values'] as $attribute => $values) {
				foreach ($attributes as $key => $value) {
					if ($key == $attribute && is_array($values) && !empty($values)) {
						if (!in_array($value, $values)) {
							throw new CHttpException(400, "Unknown attribute value: $value");
						}
					}
				}
			}
		}
		return true;
	}

	/**
	 * Validates current request method.
	 * @author andcherv
	 * @param $method
	 * @return bool
	 * @throws CHttpException
	 */
	public function validateRequestMethod($method)
	{
		if (Yii::app()->request->requestType != $method) {
			throw new CHttpException(405);
		}
		return true;
	}

}