<?php

/**
 * This is the model class for table "qApi_key".
 *
 * The followings are the available columns in table 'qApi_key':
 * @property integer $id
 * @property integer $user_id
 * @property string $key
 * @property string $secret
 * @property string $description
 * @property string $create_time
 * @property string $update_time
 * @property string $state
 *
 * The followings are the available model relations:
 * @property UserAuth $user @todo user model define in config
 */
class qApiKey extends CActiveRecord
{
	const STATE_DELETED = 'deleted';
	const STATE_DISABLED = 'disabled';
	const STATE_ACTIVE = 'active';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'qApi_key';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, key, secret, create_time, update_time, state', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('key, secret, state', 'length', 'max'=>255),
			array('create_time, update_time', 'length', 'max'=>11),
			array('description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, key, secret, description, create_time, update_time, state', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'UserAuth', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'key' => 'Key',
			'secret' => 'Secret',
			'description' => 'Description',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'state' => 'State',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('key',$this->key,true);
		$criteria->compare('secret',$this->secret,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('state',$this->state,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return qApiKey the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function afterConstruct()
	{
		if ($this->isNewRecord) {
			$this->generateKey();
			$this->generateSecret();
			$this->activate();
		}
		return true;
	}

	public function beforeValidate()
	{
		if ($this->isNewRecord) {
			$this->create_time = time();
			$this->state = 'active';
		}
		$this->update_time = time();
		return parent::beforeValidate();
	}

	public function activate()
	{
		$this->state = self::STATE_ACTIVE;
	}

	public function deactivate()
	{
		$this->state = self::STATE_DISABLED;
	}

	/**
	 * Generates API key (SHA1 hash).
	 * @todo make private
	 * @author andcherv
	 * @param bool $assign
	 * @return string
	 */
	public function generateKey($assign = true)
	{
		$apiKey = sha1(uniqid(rand()));
		if ($this->model()->find("`t`.`key`='$apiKey'") != null)
			$apiKey = $this->generateApiKey();
		if ($assign === true)
			$this->key = $apiKey;
		return $apiKey;
	}

	/**
	 * Generates API secret.
	 * @todo make private
	 * @param int $length
	 * @param bool $assign
	 * @return array|string
	 */
	public function generateSecret($length = 16, $assign = true)
	{
		$apiSecret = array();
		$alpha = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
		$alphaLength = strlen($alpha) - 1;
		for ($i = 0; $i < $length; $i++) {
			$item = rand(0, $alphaLength);
			$apiSecret[] = $alpha[$item];
		}
		$apiSecret = implode($apiSecret);
		if ($assign === true)
			$this->secret = $apiSecret;
		return $apiSecret;
	}

	/**
	 * Generates API auth hash.
	 * @author andcherv
	 * @param string $hash Request Auth-Hash.
	 * @param array $data Request data (body, session, token, uri).
	 * @param string $algo Hashing algo. Defaults to SHA1.
	 * @return bool
	 */
	public function verifyHash($hash, $data, $algo = 'sha1')
	{
		ksort($data);
		Yii::trace('Verifying the request hash ' . $hash . ' with data given: ' . CVarDumper::dumpAsString($data), 'q.api.security');
		return $hash == hash_hmac($algo, $this->key . (substr_replace(time(), '000', -3)) . implode('', $data), $this->secret) ||
		$hash == hash_hmac($algo, $this->key . (substr_replace(time(), '000', -3) + 1000) . implode('', $data), $this->secret);
	}

	public function scopes()
	{
		return array(
			'deleted' => array(array(
				'condition' => $this->tableAlias . '.state=:state',
				'params' => array(':state' => self::STATE_DELETED),
			)),
			'disabled' => array(
				'condition' => $this->tableAlias . '.state=:state',
				'params' => array(':state' => self::STATE_DISABLED),
			),
			'active' => array(
				'condition' => $this->tableAlias . '.state=:state',
				'params' => array(':state' => self::STATE_ACTIVE),
			),
		);
	}
}
