<?php

/**
 * This is the model class for table "qApi_log".
 *
 * The followings are the available columns in table 'qApi_log':
 * @property integer $id
 * @property string $client_addr
 * @property string $client_key
 * @property string $client_token
 * @property string $request_type
 * @property string $request_uri
 * @property string $request_headers
 * @property string $request_body
 * @property integer $request_time
 * @property integer $response_code
 * @property string $response_status
 * @property string $response_headers
 * @property string $response_body
 * @property float $profiling_time
 * @property integer $profiling_memory
 */
class qApiLog extends CActiveRecord
{
	const STATUS_UNKNOWN = 'unknown';
	const STATUS_ERROR = 'error';
	const STATUS_FAIL = 'fail';
	const STATUS_SUCCESS = 'success';

	const PERIOD_DAY = 'day';
	const PERIOD_WEEK = 'week';
	const PERIOD_7DAYS = '7days';
	const PERIOD_MONTH = 'month';
	const PERIOD_30DAYS = '30days';

	public $app;
	public $avg_time;
	public $avg_memory;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'qApi_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('request_time, response_code', 'numerical', 'integerOnly'=>true),
			array('client_addr, client_key, client_token, request_type, response_status, response_headers', 'length', 'max'=>255),
			array('request_uri, request_headers, request_body, response_body, profiling_time, profiling_memory', 'safe'),
			array('id, client_addr, client_key, app, client_token, request_type, request_uri, request_headers, request_body, request_time, response_code, response_status, response_headers, response_body', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'key' => array(self::BELONGS_TO, 'qApiKey', 'client_key')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'client_addr' => 'Client Addr',
			'client_key' => 'Client Key',
			'client_token' => 'Client Token',
			'request_type' => 'Request Type',
			'request_uri' => 'Request Uri',
			'request_headers' => 'Request Headers',
			'request_body' => 'Request Body',
			'request_time' => 'Request Time',
			'response_code' => 'Response Code',
			'response_status' => 'Response Status',
			'response_headers' => 'Response Headers',
			'response_body' => 'Response Body',
			'profiling_time' => 'Profiling Time',
			'profiling_memory' => 'Profiling Memory',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->with = array('key');
		$criteria->select = array(
			'*', 'key.description as app'
		);

		$criteria->compare('client_addr',$this->client_addr,true);
		$criteria->compare('client_key',$this->client_key,true);
		$criteria->compare('client_token',$this->client_token,true);
		$criteria->compare('request_type',$this->request_type,true);
		$criteria->compare('request_uri',$this->request_uri,true);
		$criteria->compare('request_headers',$this->request_headers,true);
		$criteria->compare('request_body',$this->request_body,true);
		$criteria->compare('request_time',$this->request_time);
		$criteria->compare('response_code',$this->response_code);
		$criteria->compare('response_status',$this->response_status,true);
		$criteria->compare('response_headers',$this->response_headers,true);
		$criteria->compare('response_body',$this->response_body,true);
		$criteria->compare('profiling_time',$this->profiling_time,true);
		$criteria->compare('profiling_memory',$this->profiling_memory,true);

		if (isset($this->app) && !empty($this->app))
			$criteria->addCondition("key.key LIKE '%{$this->app}%' OR key.description LIKE '%{$this->app}%'");

		$this->setProfilingVars(clone $criteria);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => Yii::app()->user->getState(get_class($this) . '_page_size', 15),
			),
			'sort'=>array(
				'defaultOrder'=>'request_time DESC',
				'attributes' => array(
					'*',
					'app' => array(
						'asc' => 't.client_key ASC',
						'desc' => 't.client_key DESC',
					),
				),
			)
		));
	}

	protected function setProfilingVars($criteria)
	{
		$criteria->select = array(
			'AVG(t.profiling_time) as avg_time',
			'AVG(t.profiling_memory) as avg_memory'
		);
		$avg = $this->find($criteria);
		if (isset($avg)) {
			$this->avg_time = $avg->avg_time;
			$this->avg_memory = $avg->avg_memory;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return qApiLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function scopes()
	{
		return array(
			'today' => array(
				'condition' => $this->tableAlias . '.request_time>:time',
				'params' => array(':time' => strtotime('today midnight UTC')),
			),
			'success' => array(
				'condition' => $this->tableAlias . '.response_status=:status',
				'params' => array(':status' => self::STATUS_SUCCESS),
			),
			'fail' => array(
				'condition' => $this->tableAlias . '.response_status=:status',
				'params' => array(':status' => self::STATUS_FAIL),
			),
			'error' => array(
				'condition' => $this->tableAlias . '.response_status=:status',
				'params' => array(':status' => self::STATUS_ERROR),
			),
			'unknown' => array(
				'condition' => $this->tableAlias . '.response_status=:status',
				'params' => array(':status' => self::STATUS_UNKNOWN),
			),
		);
	}

	public function period($period = self::PERIOD_DAY)
	{
		switch ($period) {
			case self::PERIOD_MONTH:
				$time = strtotime('first day of this month midnight UTC');
				break;
			case self::PERIOD_30DAYS:
				$time = strtotime('-30 days midnight UTC');
				break;
			case self::PERIOD_WEEK:
				$time = strtotime('monday this week midnight UTC');
				break;
			case self::PERIOD_7DAYS:
				$time = strtotime('-7 days midnight UTC');
				break;
			case self::PERIOD_DAY:
			default:
				$time = strtotime('today midnight UTC');
		}

		$this->getDbCriteria()->mergeWith(array(
			'condition' => $this->tableAlias . '.request_time>:time',
			'params' => array(':time' => $time),
		));

		return $this;
	}

	public static function getPeriodLabels()
	{
		return array(
			self::PERIOD_DAY => Yii::t('qApi', 'Today'),
			self::PERIOD_WEEK => Yii::t('qApi', 'This Week'),
			self::PERIOD_7DAYS => Yii::t('qApi', 'Last 7 Days'),
			self::PERIOD_MONTH => Yii::t('qApi', 'This Month'),
			self::PERIOD_30DAYS => Yii::t('qApi', 'Last 30 Days'),
		);
	}
}
