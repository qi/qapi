<?php

/**
 * Class DefaultController
 * @api /
 * @author chervand <chervand@gmail.com>
 * @since v.1.0.0
 */
class DefaultController extends qApiController
{
	public $allow = ['OPTIONS'];
}
