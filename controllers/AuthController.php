<?php

/**
 * Authentication controller.
 * TODO: Need factoring, move away the most logic.
 * @api /auth/
 * @author chervand <chervand@gmail.com>
 * @since v.1.0.0
 */
class AuthController extends qApiController
{
	public $allow = ['OPTIONS', 'POST', 'DELETE'];

	/**
	 * Logs the user in and returns new session ID.
	 * @throws CHttpException
	 */
	public function actionPOST()
	{
		$body = $this->module->request->body;
		if (isset($body['login']) && isset($body['password'])) {
			Yii::trace('Starting an authentication', 'q.api.security');
			$identity = new UserIdentity($body['login'], $body['password']);
			if ($identity->authenticate() && $identity->errorCode === UserIdentity::ERROR_NONE) {
				Yii::app()->user->clearStates();
				if (Yii::app()->user->login($identity)) {
					$this->render(array(
						'body' => array(
							'status' => qApiResponse::STATUS_SUCCESS,
							'data' => array(
								'session' => Yii::app()->session->sessionID,
								'token' => $this->module->token->token,
								'user' => array(
									'id' => Yii::app()->user->id
								)
							)
						)
					));
					Yii::trace('User ' . Yii::app()->user->id . ' authenticated with session: ' . Yii::app()->session->sessionID, 'q.api.security');
				} else throw new CHttpException(500, 'Login failed.');
			} else throw new CHttpException(403, 'Incorrect email or password.');
		} else {
			if (!empty(Yii::app()->session->sessionID)) {
				Yii::trace('Starting an anonymous authentication', 'q.api.security');
				Yii::trace('Regenerating the session: ' . Yii::app()->session->sessionID, 'q.api.security');
				Yii::app()->session->clear();
				Yii::app()->session->regenerateID();
				$this->render(array(
					'body' => array(
						'status' => qApiResponse::STATUS_SUCCESS,
						'data' => array(
							'session' => Yii::app()->session->sessionID,
							'token' => $this->module->token->token
						)
					)
				));
				Yii::trace('Anonymous session was set: ' . Yii::app()->session->sessionID, 'q.api.security');
			} else throw new CHttpException(400, 'Wrong cookies.');
		}
	}

	/**
	 * Logs out the user.
	 * @throws CHttpException
	 */
	public function actionDELETE()
	{
		// throw 401 if already logged out
		if (Yii::app()->session->count === 0)
			throw new CHttpException(401);

		// log out
		Yii::app()->user->logout();

		// render success
		$this->render([
			'code' => 200,
			'body' => ['status' => qApiResponse::STATUS_SUCCESS],
		]);

		// trace
		Yii::trace('User ' . Yii::app()->user->id . ' was logged out', 'q.api.security');
	}

	public function actionOPTIONS()
	{
		$this->render([
			'code' => 200,
			'headers' => ['Allow' => implode(',', $this->allow)],
			'body' => [
				'methods' => [
					'OPTIONS' => [
						'description' => 'View information on the resource.',
					],
					'POST' => [
						'description' => 'Sign in.',
						'attributes' => [
							'login' => [
								'description' => 'User\'s login',
								'demand' => 'Optional. If not set, anonymous authorization will be performed.',
							],
							'password' => [
								'description' => 'User\'s password.',
								'demand' => 'Optional. Required with login.',
							],
						],
						'example' => [
							'request' => [
								'url' => '{webroot}/api/auth',
								'headers' => 'Accept: application/json Content-Type: application/json',
								'body' => '{"login":"demo","password":"demo"}',
							],
							'response' => [
								'body' => '{"status":"success","data":{"session":"o70p3fuc13lgmqbf7ffhj9mi80","token": "f1888e6ffc5b92f10975016b5b7be6b0c2691bf7","user":{"id":"demo"}}}',
							],
						],
					],
					'DELETE' => [
						'description' => 'Sign out.',
					],
				],
			],
		]);
	}
}