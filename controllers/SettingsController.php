<?php

/**
 * API settings controller.
 * @api /settings/
 * @author chervand <chervand@gmail.com>
 * @since v.1.0.0
 */
class SettingsController extends qApiController
{
	public $allow = ['OPTIONS', 'GET'];

	public function model()
	{
		return new qApiSettings();
	}

	public function criteria()
	{
		$criteria = new CDbCriteria();
		$criteria->scopes = ['active'];
		if (isset($_GET['id'])) {
			$criteria->addSearchCondition('t.id', $_GET['id'], false);
		} else {
			if (isset($_GET['q']))
				$criteria->addSearchCondition('t.label', $_GET['q'], true, 'OR');
		}
		return $criteria;
	}

	public function data($model)
	{
		return [
			'id' => isset($model->id) ? $model->id : '',
			'type' => isset($model->type) ? $model->type : '',
			'label' => isset($model->label) ? $model->label : '',
			'attribute' => isset($model->attribute) ? $model->attribute : '',
			'value' => isset($model->value) ? $model->value : '',
		];
	}
}