<?php
/**
 * Default HEAD method. Can be overridden in a controller.
 * @author chervand <chervand@gmail.com>
 * @property qApiController $controller
 * @since v1.0.0
 */
class qApiActionHEAD extends CAction
{
	public function run()
	{
		$this->controller->render([
			'code' => 200,
		]);
	}
}