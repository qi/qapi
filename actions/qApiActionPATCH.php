<?php
/**
 * Default PATCH method. Can be overridden in a controller.
 * @author andcherv
 * @since v1.1
 */
class qApiActionPATCH extends CAction
{
	public function run($public_id)
	{
//		$this->controller->forward('PUT');
		$model = $this->controller->model();
		$attributes = $this->controller->module->request->getBody();
		if ($model instanceof CActiveRecord) {
			if (isset($attributes) && !empty($attributes)) {
				$object = $model->find(array(
					'with' => array('metadata:active'),
					'condition' => "{$model->metadataTableAlias}.public_id='{$public_id}'",
				));
				if (isset($object)) {
					$transaction = Yii::app()->db->beginTransaction();
					try {
						$object->attributes = $attributes;
						if ($object->save()) {
							$this->controller->renderUpdateSuccess($object);
							$transaction->commit();
						} else throw new CException(Yii::t('qApi', "Model update failed."));
					} catch (Exception $exception) {
						if (isset($transaction) && $transaction->active === true)
							$transaction->rollback();
						$this->controller->renderSaveErrors($object, $exception);
					}
				} else throw new CHttpException(404);
			} else throw new CHttpException(400);
		} else throw new CHttpException(501);
	}
}