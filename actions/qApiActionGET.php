<?php

/**
 * Default GET method. Can be overridden in a controller.
 * @author andcherv
 * @property qApiController $controller
 * @since v1.1
 */
class qApiActionGET extends CAction
{
	public function run($id = null, $q = null, $limit = 30, $offset = 0)
	{
		$model = $this->controller->model();
		$criteria = $this->controller->criteria();
		if ($model instanceof CActiveRecord && $criteria instanceof CDbCriteria) {
			if (!isset($id)) {
				if (isset($limit) && $limit > 0) {
					$criteria->limit = $limit;
					if (isset($offset) && $offset >= 0)
						$criteria->offset = $offset;
					$criteria->together = true;
				}
			}
			$this->controller->render([
				'body' => [
					'status' => qApiResponse::STATUS_SUCCESS,
					'pagination' => [
						'total' =>(int)$model->count($this->controller->criteria()),
						'limit' => (int)$criteria->limit,
						'offset' => (int)$criteria->offset,
					],
					'data' => array_map([$this->controller, 'data'], $model->findAll($criteria)),
				],
			]);
		} else throw new CHttpException(500);
	}
}