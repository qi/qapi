<?php

/**
 * Default DELETE method. Can be overridden in a controller.
 * @author andcherv <andcherv@gmail.com>
 * @since v1.0.0
 */
class qApiActionDELETE extends CAction
{
	public function run($id)
	{
		$model = $this->controller->model();
		if ($model instanceof CActiveRecord) {
			if ($model->exists('id=:id', array(':id' => $id))) {
				if ($model->deleteByPk($id)) {
					$this->controller->render(array(
						'code' => qApiResponse::HTTP_SUCCESS,
						'body' => array(
							'status' => qApiResponse::STATUS_SUCCESS
						)
					));
				} else throw new CHttpException(500);
			} else throw new CHttpException(404);
		} else throw new CHttpException(405);
	}
}