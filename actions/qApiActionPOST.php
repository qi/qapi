<?php

/**
 * Default POST method. Can be overridden in a controller.
 * @author chervand <chervand@gmail.com>
 * @property qApiController $controller
 * @since v1.0
 */
class qApiActionPOST extends CAction
{
	public function run()
	{
		$model = $this->controller->model();
		$attributes = $this->controller->module->request->getBody();
		if ($model instanceof CActiveRecord) {
			if (isset($attributes) && !empty($attributes)) {
				$transaction = Yii::app()->db->beginTransaction();
				try {
					$model->setAttributes($attributes);
					if ($model->save()) {
						$this->controller->renderInsertSuccess($model);
						$transaction->commit();
					} else throw new CException(Yii::t('qApi', 'Save error.'));
				} catch (Exception $exception) {
					if (isset($transaction) && $transaction->active === true)
						$transaction->rollback();
					$this->controller->renderSaveErrors($model, $exception);
				}
			} else throw new CHttpException(400);
		} else throw new CHttpException(405);
	}
}