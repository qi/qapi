<?php
/**
 * Default OPTIONS method. Can be overridden in a controller.
 * @author andcherv <andcherv@gmail.com>
 * @property qApiController $controller
 * @since v1.0.0
 */
class qApiActionOPTIONS extends CAction
{
	public function run()
	{
		$this->controller->render([
			'code' => 200,
			'headers' => [
				'Allow' => implode(',', $this->controller->allow),
			],
		]);
	}
}