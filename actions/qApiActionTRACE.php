<?php

/**
 * Default TRACE method. Can be overridden in a controller.
 * @author andcherv <andcherv@gmail.com>
 * @since v1.0.0
 */
class qApiActionTRACE extends CAction
{
	public function run()
	{
		throw new CHttpException(501);
	}
}