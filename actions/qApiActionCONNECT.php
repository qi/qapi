<?php

/**
 * Default CONNECT method. Can be overridden in a controller.
 * @author andcherv <andcherv@gmail.com>
 * @since v1.0.0
 */
class qApiActionCONNECT extends CAction
{
	public function run()
	{
		throw new CHttpException(501);
	}
}