# qApi: Yii Framework RESTful API module

[Yii Framework](http://www.yiiframework.com)

## Install

Set path aliases in /protected/config/main.php and /protected/config/console.php:

```php
<?php
	Yii::setPathOfAlias('api', dirname(dirname(__FILE__)) . '/vendor/qi/qApi');
	...
?>
```

Set the module and its components:

```php
<?php
...
'modules' => array(
	...
    'api' => array(
        'class' => 'api.ApiModule',
        'components' => array(
            'xform' => array('class' => 'qApiParserXForm'),
            'json' => array('class' => 'qApiParserJSON'),
            'xml' => array('class' => 'qApiParserXML'),
        ),
    ),
    ...
),
...
?>
```

Set urlManages rules:

```php
<?php
...
'components' => array(
	...
	'urlManager' => array(
		'urlFormat' => 'path',
		'appendParams' => false,
        'showScriptName' => false,
		'rules' => array_merge(
			array(
				// app rules
			),
			include_once(dirname(__FILE__) . '/../vendor/qi/qApi/config/rules_path.php')
		),
	),
	...
),
...
?>
```

Create DB tables:

```
$ ./protected/yiic migrate --migrationPath=api.migrations --interactive=0
```

## Configuration

### Default format

In case the request doesn't have a "Accept" header or it's set to "*/*", response's "Content-Type" will be set to "application/xml" and XML parser will be used. To change default API format set module's defaultFormat property.

```php
<?php
	...
	'modules' => array(
		...
        'api' => array(
            'class' => 'api.ApiModule',
            ...
            'defaultFormat' => 'application/json',
            ...
        ),
        ...
    ),
    ...
?>
```

## Controllers

### Mapping

Use module's [controllerPath](http://www.yiiframework.com/doc/api/1.1/CWebModule#controllerPath-detail) or [controllerMap](http://www.yiiframework.com/doc/api/1.1/CWebModule#controllerMap-detail) properties to configure API resources.

```php
<?php
	...
	'modules' => array(
		...
        'api' => array(
            'class' => 'api.ApiModule',
            ...
            'controllerPath' => dirname(__FILE__) . '/../controllers/api',
            'controllerMap' => array(
                'logs' => 'application.controllers.api.LogsController',
                'settings' => null,
            ),
            ...
        ),
        ...
    ),
    ...
?>
```

Note, that controllerMap has higher priority than controllerPath, so in case you want to override module's default controllers (default, auth, settings), you should set it with controllerMap.
