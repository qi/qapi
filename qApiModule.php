<?php

/**
 * Class qApiModule
 * @fixme Content-Type header is set to "*" by default
 * @todo move formats from settings to a config?
 * @fixme Content-Type check after action call?
 * @todo controller namespace?
 * @author andcherv
 * @property qApiRequest $request
 * @property qApiResponse $response
 */
class qApiModule extends CWebModule
{
	const COOKIE_SESSION_NAME = 'QAPISESSID';
	const COOKIE_TOKEN_NAME = 'QAPITOKEN';
	const COOKIE_PATH = '/api';
	const SESSION_TABLE_NAME = 'qApi_session';
	const SESSION_LIFETIME = 86400; // 60 * 60 * 24

	public $defaultFormat = 'application/xml';
	/**
	 * @var null|array
	 */
	public $actionMap;

	/**
	 * Available API formats.
	 * @author andcherv
	 * @var array '{MIME type}' => '{parser component name}'
	 */
	private $_formats = array('*/*' => '*');

	/**
	 * Available API security methods.
	 * @author andcherv
	 * @var array '{method}' => '{bool value}'
	 */
	private $_security = array();

	/**
	 * Initializes the API module.
	 * @author andcherv
	 */
	public function init()
	{
		Yii::app()->attachEventHandler('onException', array($this, 'errorHandler'));
		Yii::app()->import = array(
			"{$this->id}.actions.*",
			"{$this->id}.components.*",
			"{$this->id}.controllers.*",
			"{$this->id}.models.*",
		);
		$this->setComponents(array(
			'request' => array('class' => "{$this->id}.components.qApiRequest"),
			'response' => array('class' => "{$this->id}.components.qApiResponse"),
			'token' => array('class' => "{$this->id}.components.qApiToken"),
			'json' => array('class' => "{$this->id}.components.qApiParserJSON"),
			'xform' => array('class' => "{$this->id}.components.qApiParserXForm"),
			'xml' => array('class' => "{$this->id}.components.qApiParserXML"),
		));
		$this->controllerMap = CMap::mergeArray(array(
			'default' => 'api.controllers.DefaultController',
			'auth' => 'api.controllers.AuthController',
			'settings' => 'api.controllers.SettingsController',
			'keys' => 'api.controllers.KeysController',
//			'logs' => 'api.controllers.LogsController', // @todo disable logging for a controller
		), $this->controllerMap);

		// registering session component
		// @todo remove from the module
		Yii::app()->setComponent('session', array(
			'class' => 'CDbHttpSession',
			'connectionID' => 'db',
			'timeout' => self::SESSION_LIFETIME,
			'sessionName' => self::COOKIE_SESSION_NAME,
			'sessionTableName' => self::SESSION_TABLE_NAME,
			'cookieParams' => array(
				'path' => self::COOKIE_PATH,
				'lifetime' => self::SESSION_LIFETIME,
				'secure' => false,
				'httponly' => true,
			),
		));
	}

	public function getFormats()
	{
		$parsers = array();
		foreach ($this->getComponents(false) as $component => $params) {
			if ($this->{$component} instanceof qApiParser) {
				foreach ($this->{$component}->types as $type)
					$parsers[$type] = $component;
			}
		}
		return $parsers;
	}

	/**
	 * Sets module configuration for current request.
	 * TODO: remove actions 405, 501 from the default controller?
	 * @author andcherv
	 * @param CController $controller
	 * @param CAction $action
	 * @return bool
	 * @throws CHttpException
	 */
	public function beforeControllerAction($controller, $action)
	{
		if (parent::beforeControllerAction($controller, $action)) {
			if (!array_key_exists($action->id, $this->actionMap))
				throw new CHttpException(501);
			if (!$this->loadSettings() || !$this->setFormat() || !$this->request->verify())
				throw new CHttpException(500);
			if (array_search($action->id, $controller->allow) === false)
				throw new CHttpException(405);
		}
		return true;
	}

	protected function setFormat()
	{
		if (isset($this->request->accept) && $this->request->accept !== '*/*')
			$this->response->contentType = $this->request->accept;
		else
			$this->response->contentType = $this->defaultFormat;
		return true;
	}

	/**
	 * Writes logs and terminates application.
	 * @author andcherv
	 * @param CController $controller
	 * @param CAction $action
	 */
	public function afterControllerAction($controller, $action)
	{
		parent::afterControllerAction($controller, $action);
		$this->log();
		Yii::app()->end();
	}

	/**
	 * Returns array of available API formats like application/x-www-form-urlencoded,
	 * application/json, application/xml etc.
	 * @author andcherv
	 * @return array
	 */
	/*public function getFormats()
	{
		return $this->_formats;
	}*/

	/**
	 * Returns array of available security methods.
	 * @author andcherv
	 * @return array
	 */
	public function getSecurity()
	{
		return $this->_security;
	}

	/**
	 * Handles CHttpExceptions.
	 * @author andcherv
	 * @param CEvent $event
	 */
	protected function errorHandler(CEvent $event)
	{
		if ($event instanceof CExceptionEvent) {
			if ($event->exception instanceof CHttpException) {
				$code = $event->exception->statusCode ? $event->exception->statusCode : 500;
				$message = $this->response->getHttpHeader($code);
				if ($event->exception->getMessage())
					$message .= '. ' . $event->exception->getMessage();
				$this->response->render(array(
					'code' => $event->exception->statusCode,
					'headers' => array(),
					'body' => array(
						'status' => qApiResponse::STATUS_ERROR,
						'message' => "{$code} {$message}",
					),
				));
				$event->handled = true;
				$this->log();
				Yii::app()->end();
			}
		}
	}

	/**
	 * Loads settings of the module.
	 * @author andcherv
	 * @return bool False in case of DB error
	 */
	private function loadSettings()
	{
		$settings = qApiSettings::model()->active()->findAll();
		if (isset($settings)) {
			foreach ($settings as $setting) {
				$attribute = trim(strtolower($setting->attribute));
				$value = trim(strtolower($setting->value));
				switch ($setting->type) {
					case 'formats':
						$this->_formats[$attribute] = $value;
						break;
					case 'security':
						$this->_security[$attribute] = $value;
						break;
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * Saves logs to database.
	 * @author andcherv
	 */
	private function log()
	{
		$requestLog = new qApiLog();
		$requestLog->attributes = array(
			'client_addr' => $this->request->addr,
			'client_key' => $this->request->authKey,
			'client_token' => $this->request->authToken,
			'request_type' => $this->request->type,
			'request_uri' => $this->request->uri,
			'request_headers' => serialize($this->request->headers),
			'request_body' => $this->request->rawBody,
			'request_time' => $this->request->time,
			'response_code' => $this->response->code,
			'response_status' => $this->response->status,
			'response_headers' => serialize($this->response->headers),
			'response_body' => $this->response->body,
			'profiling_time' => Yii::getLogger()->getExecutionTime(),
			'profiling_memory' => Yii::getLogger()->getMemoryUsage(),
		);
		$requestLog->save(false);
	}
}
