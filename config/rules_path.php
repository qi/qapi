<?php

/**
 * qApi urlManager rules.
 * @author andcherv <andcherv@gmail.com>
 * @example host/api/controller/id
 * @example host/api/ver/controller/id
 * @example host/api/ver/controller/
 * @since v1.0.0
 */
return array(

	// default
	array('<module>/default/CONNECT', 'verb' => 'CONNECT', 'pattern' => '<module:api>'),
	array('<module>/default/DELETE', 'verb' => 'DELETE', 'pattern' => '<module:api>'),
	array('<module>/default/GET', 'verb' => 'GET', 'pattern' => '<module:api>'),
	array('<module>/default/HEAD', 'verb' => 'HEAD', 'pattern' => '<module:api>'),
	array('<module>/default/OPTIONS', 'verb' => 'OPTIONS', 'pattern' => '<module:api>'),
	array('<module>/default/PATCH', 'verb' => 'PATCH', 'pattern' => '<module:api>'),
	array('<module>/default/POST', 'verb' => 'POST', 'pattern' => '<module:api>'),
	array('<module>/default/PUT', 'verb' => 'PUT', 'pattern' => '<module:api>'),
	array('<module>/default/TRACE', 'verb' => 'TRACE', 'pattern' => '<module:api>'),

	// default + version
	array('<module>/default/CONNECT', 'verb' => 'CONNECT', 'pattern' => '<module:api>/<api_version:v\d\.\d>'),
	array('<module>/default/DELETE', 'verb' => 'DELETE', 'pattern' => '<module:api>/<api_version:v\d\.\d>'),
	array('<module>/default/GET', 'verb' => 'GET', 'pattern' => '<module:api>/<api_version:v\d\.\d>'),
	array('<module>/default/HEAD', 'verb' => 'HEAD', 'pattern' => '<module:api>/<api_version:v\d\.\d>'),
	array('<module>/default/OPTIONS', 'verb' => 'OPTIONS', 'pattern' => '<module:api>/<api_version:v\d\.\d>'),
	array('<module>/default/PATCH', 'verb' => 'PATCH', 'pattern' => '<module:api>/<api_version:v\d\.\d>'),
	array('<module>/default/POST', 'verb' => 'POST', 'pattern' => '<module:api>/<api_version:v\d\.\d>'),
	array('<module>/default/PUT', 'verb' => 'PUT', 'pattern' => '<module:api>/<api_version:v\d\.\d>'),
	array('<module>/default/TRACE', 'verb' => 'TRACE', 'pattern' => '<module:api>/<api_version:v\d\.\d>'),

	// basic methods
	array('<module>/<controller>/CONNECT', 'verb' => 'CONNECT', 'pattern' => '<module:api>/<controller:\w+>'),
	array('<module>/<controller>/DELETE', 'verb' => 'DELETE', 'pattern' => '<module:api>/<controller:\w+>'),
	array('<module>/<controller>/GET', 'verb' => 'GET', 'pattern' => '<module:api>/<controller:\w+>'),
	array('<module>/<controller>/HEAD', 'verb' => 'HEAD', 'pattern' => '<module:api>/<controller:\w+>'),
	array('<module>/<controller>/OPTIONS', 'verb' => 'OPTIONS', 'pattern' => '<module:api>/<controller:\w+>'),
	array('<module>/<controller>/PATCH', 'verb' => 'PATCH', 'pattern' => '<module:api>/<controller:\w+>'),
	array('<module>/<controller>/POST', 'verb' => 'POST', 'pattern' => '<module:api>/<controller:\w+>'),
	array('<module>/<controller>/PUT', 'verb' => 'PUT', 'pattern' => '<module:api>/<controller:\w+>'),
	array('<module>/<controller>/TRACE', 'verb' => 'TRACE', 'pattern' => '<module:api>/<controller:\w+>'),

	// basic methods + version
	array('<module>/<controller>/CONNECT', 'verb' => 'CONNECT', 'pattern' => '<module:api>/<api_version:v\d\.\d>/<controller:\w+>'),
	array('<module>/<controller>/DELETE', 'verb' => 'DELETE', 'pattern' => '<module:api>/<api_version:v\d\.\d>/<controller:\w+>'),
	array('<module>/<controller>/GET', 'verb' => 'GET',	'pattern' => '<module:api>/<api_version:v\d\.\d>/<controller:\w+>'),
	array('<module>/<controller>/HEAD', 'verb' => 'HEAD', 'pattern' => '<module:api>/<api_version:v\d\.\d>/<controller:\w+>'),
	array('<module>/<controller>/OPTIONS', 'verb' => 'OPTIONS',	'pattern' => '<module:api>/<api_version:v\d\.\d>/<controller:\w+>'),
	array('<module>/<controller>/PATCH', 'verb' => 'PATCH', 'pattern' => '<module:api>/<api_version:v\d\.\d>/<controller:\w+>'),
	array('<module>/<controller>/POST', 'verb' => 'POST', 'pattern' => '<module:api>/<api_version:v\d\.\d>/<controller:\w+>'),
	array('<module>/<controller>/PUT', 'verb' => 'PUT',	'pattern' => '<module:api>/<api_version:v\d\.\d>/<controller:\w+>'),
	array('<module>/<controller>/TRACE', 'verb' => 'TRACE', 'pattern' => '<module:api>/<api_version:v\d\.\d>/<controller:\w+>'),

	// basic methods + id
	array('<module>/<controller>/CONNECT', 'verb' => 'CONNECT', 'pattern' => '<module:api>/<controller:\w+>/<id:\w+>'),
	array('<module>/<controller>/DELETE', 'verb' => 'DELETE', 'pattern' => '<module:api>/<controller:\w+>/<id:\w+>'),
	array('<module>/<controller>/GET', 'verb' => 'GET',	'pattern' => '<module:api>/<controller:\w+>/<id:\w+>'),
	array('<module>/<controller>/HEAD', 'verb' => 'HEAD', 'pattern' => '<module:api>/<controller:\w+>/<id:\w+>'),
	array('<module>/<controller>/OPTIONS', 'verb' => 'OPTIONS', 'pattern' => '<module:api>/<controller:\w+>/<id:\w+>'),
	array('<module>/<controller>/PATCH', 'verb' => 'PATCH', 'pattern' => '<module:api>/<controller:\w+>/<id:\w+>'),
	array('<module>/default/405', 'verb' => 'POST', 'pattern' => '<module:api>/<controller:\w+>/<id:\w+>'),
	array('<module>/<controller>/PUT', 'verb' => 'PUT', 'pattern' => '<module:api>/<controller:\w+>/<id:\w+>'),
	array('<module>/<controller>/TRACE', 'verb' => 'TRACE',	'pattern' => '<module:api>/<controller:\w+>/<id:\w+>'),

	// basic methods + id + version
	array('<module>/<controller>/CONNECT', 'verb' => 'CONNECT',	'pattern' => '<module:api>/<api_version:v\d\.\d>/<controller:\w+>/<id:\w+>'),
	array('<module>/<controller>/DELETE', 'verb' => 'DELETE', 'pattern' => '<module:api>/<api_version:v\d\.\d>/<controller:\w+>/<id:\w+>'),
	array('<module>/<controller>/GET', 'verb' => 'GET', 'pattern' => '<module:api>/<api_version:v\d\.\d>/<controller:\w+>/<id:\w+>'),
	array('<module>/<controller>/HEAD', 'verb' => 'HEAD', 'pattern' => '<module:api>/<api_version:v\d\.\d>/<controller:\w+>/<id:\w+>'),
	array('<module>/<controller>/OPTIONS', 'verb' => 'OPTIONS',	'pattern' => '<module:api>/<api_version:v\d\.\d>/<controller:\w+>/<id:\w+>'),
	array('<module>/<controller>/PATCH', 'verb' => 'PATCH', 'pattern' => '<module:api>/<api_version:v\d\.\d>/<controller:\w+>/<id:\w+>'),
	array('<module>/default/405', 'verb' => 'POST', 'pattern' => '<module:api>/<api_version:v\d\.\d>/<controller:\w+>/<id:\w+>'),
	array('<module>/<controller>/PUT', 'verb' => 'PUT',	'pattern' => '<module:api>/<api_version:v\d\.\d>/<controller:\w+>/<id:\w+>'),
	array('<module>/<controller>/TRACE', 'verb' => 'TRACE',	'pattern' => '<module:api>/<api_version:v\d\.\d>/<controller:\w+>/<id:\w+>'),
);