<?php

/**
 * qApi rules.
 * Requires apache ServerAlias and hosts mapping set to this subdomain.
 * @author andcherv
 * @example api.host/controller/id
 * @example api.host/ver/controller/id
 * @example api.host/ver/controller/
 * @since v1.1
 */
return array(

	// default
	array('<module>',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})'),

	// default + version
	array('<module>',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<api_version:v\d\.\d>'),

	// basic methods
	array('<module>/<controller>/CONNECT', 'verb' => 'CONNECT',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<controller:\w+>'),
	array('<module>/<controller>/DELETE', 'verb' => 'DELETE',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<controller:\w+>'),
	array('<module>/<controller>/GET', 'verb' => 'GET',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<controller:\w+>'),
	array('<module>/<controller>/HEAD', 'verb' => 'HEAD',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<controller:\w+>'),
	array('<module>/<controller>/OPTIONS', 'verb' => 'OPTIONS',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<controller:\w+>'),
	array('<module>/<controller>/PATCH', 'verb' => 'PATCH',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<controller:\w+>'),
	array('<module>/<controller>/POST', 'verb' => 'POST',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<controller:\w+>'),
	array('<module>/<controller>/PUT', 'verb' => 'PUT',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<controller:\w+>'),
	array('<module>/<controller>/TRACE', 'verb' => 'TRACE',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<controller:\w+>'),

	// basic methods + version
	array('<module>/<controller>/CONNECT', 'verb' => 'CONNECT',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<api_version:v\d\.\d>/<controller:\w+>'),
	array('<module>/<controller>/DELETE', 'verb' => 'DELETE',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<api_version:v\d\.\d>/<controller:\w+>'),
	array('<module>/<controller>/GET', 'verb' => 'GET',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<api_version:v\d\.\d>/<controller:\w+>'),
	array('<module>/<controller>/HEAD', 'verb' => 'HEAD',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<api_version:v\d\.\d>/<controller:\w+>'),
	array('<module>/<controller>/OPTIONS', 'verb' => 'OPTIONS',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<api_version:v\d\.\d>/<controller:\w+>'),
	array('<module>/<controller>/PATCH', 'verb' => 'PATCH',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<api_version:v\d\.\d>/<controller:\w+>'),
	array('<module>/<controller>/POST', 'verb' => 'POST',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<api_version:v\d\.\d>/<controller:\w+>'),
	array('<module>/<controller>/PUT', 'verb' => 'PUT',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<api_version:v\d\.\d>/<controller:\w+>'),
	array('<module>/<controller>/TRACE', 'verb' => 'TRACE',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<api_version:v\d\.\d>/<controller:\w+>'),

	// basic methods + public_id
	array('<module>/<controller>/CONNECT', 'verb' => 'CONNECT',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<controller:\w+>/<public_id:\w+>'),
	array('<module>/<controller>/DELETE', 'verb' => 'DELETE',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<controller:\w+>/<public_id:\w+>'),
	array('<module>/<controller>/GET', 'verb' => 'GET',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<controller:\w+>/<public_id:\w+>'),
	array('<module>/<controller>/HEAD', 'verb' => 'HEAD',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<controller:\w+>/<public_id:\w+>'),
	array('<module>/<controller>/OPTIONS', 'verb' => 'OPTIONS',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<controller:\w+>/<public_id:\w+>'),
	array('<module>/<controller>/PATCH', 'verb' => 'PATCH',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<controller:\w+>/<public_id:\w+>'),
	array('<module>/<controller>/POST', 'verb' => 'POST',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<controller:\w+>/<public_id:\w+>'),
	array('<module>/<controller>/PUT', 'verb' => 'PUT',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<controller:\w+>/<public_id:\w+>'),
	array('<module>/<controller>/TRACE', 'verb' => 'TRACE',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<controller:\w+>/<public_id:\w+>'),

	// basic methods + public_id + version
	array('<module>/<controller>/CONNECT', 'verb' => 'CONNECT',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<api_version:v\d\.\d>/<controller:\w+>/<public_id:\w+>'),
	array('<module>/<controller>/DELETE', 'verb' => 'DELETE',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<api_version:v\d\.\d>/<controller:\w+>/<public_id:\w+>'),
	array('<module>/<controller>/GET', 'verb' => 'GET',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<api_version:v\d\.\d>/<controller:\w+>/<public_id:\w+>'),
	array('<module>/<controller>/HEAD', 'verb' => 'HEAD',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<api_version:v\d\.\d>/<controller:\w+>/<public_id:\w+>'),
	array('<module>/<controller>/OPTIONS', 'verb' => 'OPTIONS',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<api_version:v\d\.\d>/<controller:\w+>/<public_id:\w+>'),
	array('<module>/<controller>/PATCH', 'verb' => 'PATCH',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<api_version:v\d\.\d>/<controller:\w+>/<public_id:\w+>'),
	array('<module>/<controller>/POST', 'verb' => 'POST',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<api_version:v\d\.\d>/<controller:\w+>/<public_id:\w+>'),
	array('<module>/<controller>/PUT', 'verb' => 'PUT',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<api_version:v\d\.\d>/<controller:\w+>/<public_id:\w+>'),
	array('<module>/<controller>/TRACE', 'verb' => 'TRACE',
		'pattern' => $_SERVER['REQUEST_SCHEME'] . '://<module:api>.([\w\.\-]{1,})/<api_version:v\d\.\d>/<controller:\w+>/<public_id:\w+>'),
);